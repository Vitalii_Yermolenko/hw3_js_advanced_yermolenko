function getResultOnWindow(obj,parent){
  const taskUl = document.createElement('ul');
  parent.append(taskUl);
  for (const key in obj) {
    if(typeof(obj[key]) != 'object'){
    const taskLi = document.createElement('li');
      taskLi.innerText = `${key}:${obj[key]}`
      taskUl.append(taskLi);
    } else {
      const taskLi = document.createElement('li');
      taskLi.innerText = `${key}:`
      taskUl.append(taskLi);
      getResultOnWindow(obj[key],taskLi)
    }

  }
}


// Task 1
/////////////////////
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let company = [...new Set([...clients1,...clients2])];

const task1 =document.createElement('div');
document.body.append(task1);
const task1Title = document.createElement('h2');
task1.append(task1Title);
task1Title.innerText ='Task1.';
const task1Text = document.createElement('p');
task1.append(task1Text);
task1Text.innerText = company;
// Task 2
/////////////////////
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

const charactersShortInfo = [];
const task2 =document.createElement('div');
document.body.append(task2);
const task2Title = document.createElement('h2');
task2Title.innerText = 'Task2.';
task2.append(task2Title);

  characters.forEach(element => {
    const {name: name2, lastName: lastName, age:age2} = element;
    charactersShortInfo.push({name: name2, lastName: lastName, age:age2});
    getResultOnWindow({name: name2, lastName: lastName, age:age2},task2);
  });

// Task 3
/////////////////////
const user1 = {
    name: "John",
    years: 30
  };

  const { name: name, years: age, isAdmin: isAdmin = false } = user1;
  const task3 =document.createElement('div');
document.body.append(task3);
const task3Title = document.createElement('h2');
task3.append(task3Title);
task3Title.innerText ='Task3.';
const task3Text = document.createElement('p');
task3.append(task3Text);
task3Text.innerText =`name:${name}; age:${age}; isAdmin: ${isAdmin}`;

// Task 4
/////////////////////
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }

  const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
  const task4 =document.createElement('div');
document.body.append(task4);
const task4Title = document.createElement('h2');
task4.append(task4Title);
task4Title.innerText ='Task4.';
getResultOnWindow(fullProfile,task4);


// Task 5
/////////////////////
const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

const {name:name5, author:author} = bookToAdd;
let booksCopy = books.concat({name:name5, author:author});

const task5 =document.createElement('div');
document.body.append(task5);
const task5Title = document.createElement('h2');
task5.append(task5Title);
task5Title.innerText ='Task5.';
const task5Subtitle = document.createElement('h4');
task5.append(task5Subtitle);
task5Subtitle.innerText ='Start Massive:';
books.forEach(elem5 => {
  getResultOnWindow(elem5,task5);
});
const task5Subtitle2 = document.createElement('h4');
task5.append(task5Subtitle2);
task5Subtitle2.innerText ='Finish Massive:';
booksCopy.forEach(element5 => {
  getResultOnWindow(element5,task5);
});

// Task 6
/////////////////////
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}


const newemployee = {...employee,age:newage = "51 year", salary:newsalary = "60000 grn"};

console.log(newemployee);

const task6 =document.createElement('div');
document.body.append(task6);
const task6Title = document.createElement('h2');
task6.append(task6Title);
task6Title.innerText ='Task6.';
  const task6Subtitle = document.createElement('h4');
  task6.append(task6Subtitle);
  task6Subtitle.innerText ='Start Object:';
  getResultOnWindow(employee,task6);
    const task6Subtitle2 = document.createElement('h4');
    task6.append(task6Subtitle2);
    task6Subtitle2.innerText ='Finish Object:';
    getResultOnWindow(newemployee,task6);


// Task 7
/////////////////////

const array = ['value', () => 'showValue'];
let [value,showValue] = array;
// Допишіть код тут

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'
